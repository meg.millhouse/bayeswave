ARG BUILD_IMAGE
FROM $BUILD_IMAGE
ARG BUILD_DATE
ARG CI_COMMIT_SHA

# http://label-schema.org/rc1/
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="bayeswave-runtime"
LABEL org.label-schema.description="BayesWave: distinguish gravitational wave signals from noise and instrumental glitches"
LABEL org.label-schema.url="https://docs.ligo.org/lscsoft/bayeswave/"
LABEL org.label-schema.vcs-url="https://git.ligo.org/lscsoft/bayeswave"
LABEL org.label-schema.build-date="${BUILD_DATE}"
LABEL org.label-schema.vcs-ref="${CI_COMMIT_SHA}"

# Copy all the source into /tmp which we will empty later
COPY . /tmp

RUN cd /tmp && \
      mkdir -p build && \
      pushd build &&  \
      cmake .. \
      -DCMAKE_INSTALL_PREFIX= \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_EXPORT_COMPILE_COMMANDS=true && \
      cmake --build . -- VERBOSE=1 && \
      cmake --build . --target install && \
      popd && \
      pip install --no-cache /tmp/BayesWaveUtils && \
      rm -rf /tmp/*

## Directories we may want to bind
RUN mkdir -p /cvmfs /hdfs /hadoop /etc/condor /test

COPY docker-entrypoint.sh /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
