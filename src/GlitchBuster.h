/**************************************************************************
 
 Copyright (c) 2019 Neil Cornish
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 ************************************************************************/


#ifndef GlitchBuster_h
#define GlitchBuster_h

#include <stdio.h>

/*
 *
 * Spec Functions
 *
 */

#include <gsl/gsl_sort_double.h>
#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>

#define sthresh 9.0
#define warm1 7.0
#define warm2 6.0


void GlitchBusterSpecWrapper(double *data, int N, double Tobs, double fmax, double dt, double *SN, double *SM, double *PS);

void specest(double *data, int N, int Ns, double dt, double fmax, double *SN, double *SM, double *PS);

//void tukey(double *data, double alpha, int N);
//void tukey_scale(double *s1, double *s2, double alpha, int N);

void clean(double *D, double *Draw, double *sqf, double *freqs, double *Sn, double *specD, double *sspecD, double df, double Q, double Tobs, double scale, double alpha, int Nf, int N, int imin, int imax, double *SNR, int pflag);

void spectrum(double *data, double *S, double *Sn, double *Smooth, double df, int N);

void layerC(double *a, double f, double *tf, double *tfR, double *tfI, double Q, double Tobs, double fix, int n);

void SineGaussianC(double *hs, double *sigpar, double Tobs, int N, int *imn, int *imx);


void TransformC(double *a, double *freqs, double **tf, double **tfR, double **tfI, double Q, double Tobs, int n, int m);

double Getscale(double *freqs, double Q, double Tobs, double fmax, int n, int m);


/*
 *
 * GlitchStart Functions
 *
 */

#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>
#include <gsl/gsl_linalg.h>

#define thresh  9.0    // SNR^2 threshold per pixel
#define cthresh 16.0   // SNR^2 threshold per cluster
#define NWP 5           // number of wavelet parameters
#define NUMBER_Q_LAYERS 6           // number of Q layers;
#define cdmax 8.0       // cluster distance threshold
#define tuke 0.4       // Tukey window rise (s)
#define fmn 8.0       // minimum frequency

double **GlitchBusterWrapper(int m, int N, double Tobs, double ttrig, double fmax, double *SN, double *ASD, double *data, int *Nwavelet, int Dmax);

double tfdistance(double *par1, double *par2);

void Inverse(double **M, double **IM, int d);

void SineGaussianW(double *hs, double *sigpar, double *ASD, double Tobs, int N, int *imn, int *imx);

static double f_nwip_imin_imax(double *a, double *b, int n, int imin, int imax);

void flip(double *a, double *af, int n);

double nwip(double *a, double *b, double *Sn, int n);

void layer(double *a, double f, double *ASD, double *tf, double *tfnorm, double *tfphase, double Q, double Tobs, double fix, int n);

void Transform(double *a, double *freqs, double *ASD, double **tf, double *tfnorm, double **tfphase, double Q, double Tobs, int n, int m);

void deltaTransform(int ii, int jj, double *a, double *freqs, double *ASD, double **tf, double *tfnorm, double **tfphase, double Q, double Tobs, int n, int m);

static void recursive_phase_evolution_glitchbuster(double dre, double dim, double *cosPhase, double *sinPhase);


/*
 *
 * Common to both Spec and GlitchStart
 *
 */

void whiten(double *data, double *ASD, int N);

void SineGaussianFreq(double *hs, double *sigpar, double Tobs, int N);

#endif /* GlitchBuster_h */
